package ca1part2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Random;

/**
 *sources https://www.javacodeexamples.com/java-arraylist-shuffle-elements-example/969
 * used this when new Random(6) wasn't working with the Collections.shuffle
 * @author Aidan Bligh
 */
public class Q5_Quiz 
{
    Scanner kb;
    ArrayList <String> questions = new ArrayList<String>();
    ArrayList <String> answers = new ArrayList<String>();
    Random rand = new Random();

    /**
     *Makes quiz class, randomizes questions
     */
    public Q5_Quiz() 
    {
        this.kb = new Scanner(System.in);
        //questions
        questions.add("How would we not know if space vampires existed?");
        questions.add("What is your quest?");
        questions.add("What is your favourite color?");
        questions.add("What is the capital of Assyria?");
        questions.add("What is the air-speed velocity of an unladen swallow? ");
        questions.add("If people 'dress up' as clowns do clowns really exist?");
        
        //answers
        answers.add("Because telescopes use mirrors");
        answers.add("To seek the holy grail");
        answers.add("Blue");
        answers.add("I don't know");
        answers.add("An African or European swallow?");
        answers.add("I don't know just give me my hot dog");
        //used linked source here
        long seed = System.currentTimeMillis();
        Collections.shuffle(questions, new Random(seed));
        Collections.shuffle(answers, new Random(seed));
    }
    
    /**
     *prints questions of current instance of quiz takes in answers and returns 
     * count of how many questions user guessed correctly
     */
    public void askQuestion()
    {
        
        int correctCount = 0;
        String ans;
        for(int i =0; i<questions.size();i++)
        {
            System.out.println("------------------------------------------------");
            System.out.println(questions.get(i));
            ans = kb.nextLine();
            if(ans.equalsIgnoreCase(answers.get(i)))
            {
                System.out.println("Correct");
                correctCount++;
            }
            else
            {
                System.out.println("Incorrect the answer is the following:");
                System.out.println(answers.get(i));
            }
        }
        System.out.println("Quiz complete, you got " +  correctCount + " questions correct out of " + questions.size());
    }

}
