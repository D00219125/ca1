package Ca1;

/**
 *
 * @author Aidan Bligh
 */
public class Matrix {
    private int row;
    private int column;
    private int [] [] matrix = new int [row][column];
    //constructor

    public Matrix(int row, int column, int[][] matrix) {
        this.row = row;
        this.column = column;
        this.matrix = matrix;
    }
    
    //getters

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public int[][] getMatrix() {
        return matrix;
    }

    
    //setters

    public void setRow(int row) {
        this.row = row;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }

    
    
    public void addMatricies(Matrix m1,Matrix m2)
    {
        if(m1.getRow()!=m2.getRow() || m1.getColumn()!=m2.getColumn())
        {
            System.out.println("The matricies must have the same numbers of rows and columns");
        }
        else
        {
            for(int i = 0; i<m1.getRow();i++)
            {
                for(int j = 0; j<m1.getColumn();j++)
                {
                    //System.out.println(m1[i][j]);
                }
            }
        }
    }
}
