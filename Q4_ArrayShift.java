package ca1part2;

import java.util.Arrays;
import static java.util.Arrays.copyOfRange;
import static java.util.Arrays.fill;
import java.util.Scanner;

/**
 *Q4 Array shift shifts all elements of an integer array the the right by n places
 * @author Aidan Bligh
 */
public class Q4_ArrayShift {
    
    Scanner kb = new Scanner(System.in);
     
    //Fill array from q3

    /**
     *fills an array wit the length of the users choice with values of the users choice
     * @return int[ users chosen length]a filled with users values
     */
    public int [] fillArray()
    {
        System.out.println("What size would you like your Array to be?");
        int size = kb.nextInt();
        while(size<=0)
        {
            System.out.println("Your array can't be that small. please enter a size over 0");
            size = kb.nextInt();
        }
        int [] a = new int [size];
        for(int i = 0; i<size;i++)
        {
            System.out.println("Enter the  value for " + (i+1));
            a[i] = kb.nextInt();
        }
        return a;
    }
    
    /**
     *shifts all elements of an integer array (a) the the right by n places
     * @param a
     * @param n
     * @return
     */
    public int [] circularShiftRight(int a[], int n)
    {
        int [] result = new int [a.length];
        int [] temp = copyOfRange(a,0,n);
        
        //System.out.println("temp = " + Arrays.toString(temp));
        if(a.length < n)
        {
            System.out.println("n is greater than the length of your Array");
            fill(result,-1);
            return result;
        }
        else
        { 
            
            for(int i = 0;i<a.length;i++)
            {
                if(i+n<a.length)
                {
                    result[i] = a[i+n];
                    //System.out.println("Result = " + Arrays.toString(result));
                }
                else
                {
                    result[i] = a[i+n-a.length];
                    //System.out.println("Result = " + Arrays.toString(result));
                }
                
                
            }
        }
        return result;
    }
}
