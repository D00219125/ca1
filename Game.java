package ca1part2;

import java.util.ArrayList;

/**
 *
 * @author Aidan Bligh
 */
public class Game {
    ArrayList<Level> levels = new ArrayList<Level>();

    /**
     *Adds level to array list of levels
     * @param name
     */
    public void addLevel(String name)
    {
        Level newLevel = new Level(name);
        levels.add(newLevel);
        System.out.println("Level " + name + " added to list");
    }

    /**
     *adds player to particular level, player has a name and health
     * @param levelIndex
     * @param name
     * @param health
     */
    public void addPlayer(int levelIndex, String name, int health)
    {
        if(health<=0)
        {
            System.out.println("Health cant be negative. " +name + " not added");
        }
        else
        {
            levels.get(levelIndex).addPlayer(name, health);
            levels.get(levelIndex).checkPlayerHealth(levelIndex);
            System.out.println("Player '"+ name + "' with " + health +" health added to " + levels.get(levelIndex).getLevelName());
        }
    }

    /**
     *updates particular players (playerIndex) health in a particular level (levelIndex) by change
     * @param levelIndex
     * @param playerIndex
     * @param change
     */
    public void updatePlayersHealth(int levelIndex, int playerIndex, int change)
    {
        //need playername for dead player
        String playerName = levels.get(levelIndex).players.get(playerIndex).getName();
        levels.get(levelIndex).updatePlayerHealth(playerIndex, change);
        if(levels.get(levelIndex).players.get(playerIndex).getHealth()>0)
        {
            System.out.println("Player "+playerIndex+":" +levels.get(levelIndex).players.get(playerIndex).getName()
                +"'s health on level " + levels.get(levelIndex).getLevelName()+
                "is now " + levels.get(levelIndex).players.get(playerIndex).getHealth());
        }
        else
        {
            System.out.println("Player " + playerName + " is now Dead");
        }
        
    }

    /**
     *Prints list of players in a particular level (levelIndex)
     * @param levelIndex
     */
    public void listPlayers(int levelIndex)
    {
        System.out.println("--------------------------------------------------------");
        System.out.println("List of all the players on " + levels.get(levelIndex).getLevelName());
        //System.out.println("Name\t\tHealth");
        for(int i = 0; i<levels.get(levelIndex).players.size();i++)
        {
            System.out.println("Player " +i +": "+levels.get(levelIndex).players.get(i).getName()+"\tHealth: " + levels.get(levelIndex).players.get(i).getHealth());
        }
        System.out.println("--------------------------------------------------------");
    }
}