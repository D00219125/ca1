package ca1part2;

import java.util.ArrayList;

/**
 *
 * @author Aidan Bligh
 */
public class Level {
    ArrayList<Player> players = new ArrayList<Player>();
    private String levelName;

    /**
     *Creates instance of level
     * @param levelName
     */
    public Level(String levelName) {
        this.levelName = levelName;
    }

    /**
     *returns level name as string
     * @return String levelName
     */
    public String getLevelName() {
        return levelName;
    }
    
    /**
     *adds player to current instance of level
     * @param name
     * @param health
     */
    public void addPlayer(String name, int health)
    {
        Player newPlayer = new Player(name, health);
        players.add(newPlayer);
    }

    /**
     *removes player if dead, prints players health if they're alive
     * @param playerIndex
     */
    public void checkPlayerHealth(int playerIndex)
    {
        if(players.get(playerIndex).getHealth()<=0)
        {
            System.out.println("Player " +players.get(playerIndex).getName()+ " is dead removing from level.");
            players.remove(playerIndex);
        }
        else
        {
            System.out.println(players.get(playerIndex).getName()+"'s health is " + players.get(playerIndex).getHealth());
        }
    }
    
    //updates in positive or negatave values

    /**
     *adds change parameter to player(needs -(minus) if subtracting health)
     * @param playerIndex
     * @param change
     */
    
    public void updatePlayerHealth(int playerIndex, int change)
    {
        int health = players.get(playerIndex).getHealth();
        players.get(playerIndex).setHealth(health += change);
        checkPlayerHealth(playerIndex);
    }
    
    //showing off autoboxing

    /**
     *demonstrates autoboxing
     * @param playerIndex
     * @param change
     */
    public void updatePlayerHealthAutobox(int playerIndex, int change)
    {
        Integer health = players.get(playerIndex).getHealth();//autoboxing
        players.get(playerIndex).setHealth(health += change);
        checkPlayerHealth(playerIndex);
    }
    //unboxing

    /**
     *demonstrates unboxing
     * @param playerIndex
     * @param change
     */
    public void updatePlayerHealthUnboxing(int playerIndex, int change)
    {
        Integer health = players.get(playerIndex).getHealth();
        int hp = health;    //unboxing
        players.get(playerIndex).setHealth(hp += change);
        checkPlayerHealth(playerIndex);
    }
    
}
