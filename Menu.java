package ca1part2;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Aidan Bligh
 */
public class Menu {
    Scanner kb = new Scanner(System.in);

    /**
     *runs menu, prints choices, runs question the user chooses
     */
    public void menu()
    {
        int choice = 0;
        while (choice!=4)
        {
            printOptions();
            choice = kb.nextInt();
            switch(choice)
            {
                case 1:
                    q4();
                    break;
                case 2:
                    q5();
                    break;
                case 3:
                    q6();
                    break;
            }
        }
    }
    
    /**
     *Prints questions the user can run
     */
    public void printOptions()
    {
        
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Enter 1 for question 4 (circular shift right)");
        System.out.println("Enter 2 for question 5 (quiz)");
        System.out.println("Enter 3 for question 6 (Game)");
        System.out.println("Enter 4 to quit.");
        System.out.println("--------------------------------------------------------------------------------");
    }
    
    /**
     *runs question 4 using user input
     */
    public void q4()
    {
        Q4_ArrayShift q4 = new Q4_ArrayShift();
        int a[] = q4.fillArray();
        System.out.println("Enter your n index where you like to shift the elements to the right?");
        int n = kb.nextInt();
        int [] result = q4.circularShiftRight(a, n);
        System.out.println(Arrays.toString(result));
    }
        
    /**
     *Runs question 5 Quiz
     */
    public void q5()
    {
        Q5_Quiz quiz = new Q5_Quiz();
        quiz.askQuestion();
    }

    /**
     *Runs question 6 Game
     */
    public void q6()
    {
        //demonstrating methods auto & unboxing are in level class
        Game game = new Game();
        game.addLevel("Rust");
        game.addPlayer(0, "Tracer", 100);
        game.addPlayer(0, "genji", 50);
        game.addPlayer(0, "mcree", -10);
        game.listPlayers(0);
        game.updatePlayersHealth(0, 0, -99);
        game.updatePlayersHealth(0, 0, -1);
        game.listPlayers(0);
    }
}
