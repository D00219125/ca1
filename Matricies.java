package Ca1;

import java.util.Random;
import java.util.Scanner;
/**
 *
 * @author Aidan Bligh
 */

public class Matricies {
    private static Scanner kb = new Scanner(System.in);
    //4 rows 3 colums
    private int rows;
    private int columns;
    int [] [] matrix;
    
    
    public Matricies(int rows, int columns) 
    {
        int [] [] matrix = new int [rows][columns];
        for(int i = 0; i<rows;i++)
        {
            for(int j = 0; j<columns;j++)
            {
                matrix[i][j] = 1;
            }
        }
    }
    //getters
    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }
    
    public int getMatrixValue(int row, int column) {
        return matrix[row][column];
    }
    
    //setters
    public void setRowAmount(int amount)
    {
        this.rows = amount;
    }
    public void setColumnsAmount(int amount)
    {
        this.columns = amount;
    }

    public void setMatrix(int[][] matrix) {
        this.matrix = matrix;
    }
    
//    //set value of matrix
//    public void setMatrixValues(Matricies m)
//    {
//        for(int i = 0; i<m.getRows();i++)
//        {
//            for(int j = 0; j<m.getColumns();j++)
//            {
//                System.out.println("Enter the value for row "+ (i+1) + " column " + (j+1) );
//                m.matrix[i][j] = kb.nextInt();
//            }
//        }
//    }
    //add two matxicies. returns a new matrix
    public Matricies addMatricies(Matricies m1, Matricies m2)
    {
        Matricies newMatrix = new Matricies(m1.getRows(),m1.getColumns());
        if(m1.getRows()!=m2.getRows() || m1.getColumns()!=m2.getColumns())
        {
            System.out.println("The matricies must have the same numbers of rows and columns");
        }
        else
        {
            for(int i = 0; i<newMatrix.getRows();i++)
            {
                for(int j = 0; j<newMatrix.getColumns();j++)
                {
                    matrix [i][j] = m1.getMatrixValue(i, j) + m2.getMatrixValue(i, j);
                }
            }
        }
        return newMatrix;
    }
    
    public void printMatrix(Matricies m)
    {
        for(int i = 0; i<m.getRows();i++)
        {
            for(int j = 0; j<m.getColumns();j++)
            {
                System.out.println("Row "+(i+1)+" = " + m.getMatrixValue(i,j));
            }
        }
    }
}
