package ca1;

import java.util.Arrays;
import java.util.Random;

/**
 *Matrix Class.
 * @author Aidan Bligh
 */
public class Q2_Matrix 
{
    Random rand = new Random();
    
    /**
     *Used to initialize matrix.
     * @param matrixVals
     */
    public Q2_Matrix(int [] [] matrixVals) {
        
    }
    //part a

    /**
     *Adds 2 4x3 matrices together.
     * @param m1 A matrix(2d Array)
     * @param m2 A matrix(2d Array)
     * @return A matrix(2d Array)
     */
    public static int [] [] addMatricies(int [][] m1, int [] [] m2)
    {
        int [] [] matrix3 = new int [4][3];
        for(int i = 0; i<4;i++)
        {
            for(int j = 0; j<3;j++)
            {
                matrix3[i][j] = m1[i][j] + m2[i][j];
            }
        }
        return matrix3;
    }
    //part b

    /**
     *Adds 2 matrices of any size together.
     * @param m1 A matrix(2d Array)
     * @param m2 A matrix(2d Array)
     * @param rows An integer that set's the amount of rows the matrices have
     * @param columns An integer that set's the amount of columns the matrices have
     * @return A matrix whose values are  m1's added to m2's
     */
    public static int [] [] addMatricies(int [][] m1, int [] [] m2, int rows, int columns)
    {
        int [] [] matrix3 = new int [rows][columns];
        for(int i = 0; i<rows;i++)
        {
            for(int j = 0; j<columns;j++)
            {
                matrix3[i][j] = m1[i][j] + m2[i][j];
            }
        }
        return matrix3;
    }
    
    /**
     *Prints a matrix.
     * @param vals A matrix(2d Array)
     */
    public static void printMatrix(int [] [] vals)
    {
        System.out.println(Arrays.deepToString(vals));
    }
    

}
