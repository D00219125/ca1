package ca1;

import java.util.Scanner;

/**
 *Array Class
 * @author Aidan Bligh
 */
public class Q3_Array {
        Scanner kb = new Scanner(System.in);
    private int [] a;
    
    /**
     *asks user the array size and it's values.
     * @return An integer array.
     */
    public int [] fillArray()
    {
        System.out.println("What size would you like your Array to be?");
        int size = kb.nextInt();
        while(size<=0)
        {
            System.out.println("Your array can't be that small. please enter a size over 0");
            size = kb.nextInt();
        }
        int [] a = new int [size];
        for(int i = 0; i<size;i++)
        {
            System.out.println("Enter the  value for " + (i+1));
            a[i] = kb.nextInt();
        }
        return a;
    }

    /**
     *
     * @param a An array
     * @return An array containing the index of the minimum and maximum values 
     * of a
     */
    public int [] indexOfMinAndMax(int [] a)
    {
        int minIndex = 0;
        int maxIndex = 0;
        
        for(int i = 0; i<a.length;++i)
        {
            if(a[i]<a[minIndex])
            {
                minIndex = i;
            }
            if(a[i]>a[maxIndex])
            {
                maxIndex = i;
            }
        }
        int[] ret = {minIndex,maxIndex};
        return ret;
    }


}
