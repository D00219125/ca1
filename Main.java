package ca1;

/**
 *Main
 * @author Aidan Bligh
 */
public class Main {
    
    /**
     *Initializes menu class and runs it, menu isn't in main to avoid static errors.
     * @param args
     */
    public static void main(String[] args) {
        Q1_Menu menu = new Q1_Menu();
        menu.menu();
    }
}
