package ca1part2;

import java.util.Arrays;

/**
 *Main runs menu
 * @author Aidan Bligh
 */
public class Ca1Part2 {

    /**
     *Runs main class
     * @param args
     */
    public static void main(String[] args) 
    {
        Menu menu = new Menu();
        menu.menu();
    }

}
