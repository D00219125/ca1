package ca1;

import java.util.Random;
import java.util.Arrays;
/**
 *
 * @author Aidan Bligh
 */
public class SimpleMatrix {
    public static void main(String[] args) {
        Random rand = new Random();
        
        int [] [] matrix1 = new int [4][3];
        for(int i = 0; i<4;i++)
        {
            for(int j = 0; j<3;j++)
            {
                matrix1[i][j] = rand.nextInt(10);
            }
        }
        System.out.println(Arrays.deepToString(matrix1));
        //prints m1
//        for(int i = 0; i<4;i++)
//        {
//            for(int j = 0; j<3;j++)
//            {
//                System.out.println(matrix1[i][j]);
//                if(j==0)
//                {
//                    System.out.print("{" + matrix1[i][j]+", ");
//                }
//                if(j>1)
//                {
//                    System.out.print( matrix1[i][j]+"}");
//                }
//                else
//                {
//                    System.out.print(matrix1[i][j] +", ");
//                }
//                System.out.println(" ");
//            }
//        }
        System.out.println("Matrix2");
        int [] [] matrix2 = new int [4][3];
        for(int i = 0; i<4;i++)
        {
            for(int j = 0; j<3;j++)
            {
                matrix2[i][j] = rand.nextInt(10);
            }
        }
        //prints m2
//        for(int i = 0; i<4;i++)
//        {
//            for(int j = 0; j<3;j++)
//            {
//                System.out.println(matrix2[i][j]);
//            }
//        }
        System.out.println(Arrays.deepToString(matrix2));
        int [] [] matrix3 = addMatricies(matrix1, matrix2);
        System.out.println("Matrix 3");
//        for(int i = 0; i<4;i++)
//        {
//            for(int j = 0; j<3;j++)
//            {
//                System.out.println(matrix3[i][j]);
//            }
//        }
        System.out.println(Arrays.deepToString(matrix3));
        
}
    public static int [] [] addMatricies(int [][] m1, int [] [] m2)
    {
        int [] [] matrix3 = new int [4][3];
        for(int i = 0; i<4;i++)
        {
            for(int j = 0; j<3;j++)
            {
                matrix3[i][j] = m1[i][j] + m2[i][j];
            }
        }
        return matrix3;
    }
    
    public static void printMatrix(int [] [] m1)
    {
        for(int i = 0; i<4;i++)
        {
            for(int j = 0; j<3;j++)
            {
                System.out.println("{");
                if(j==0)
                {
                    System.out.print(m1[i][j] + "\t ");
                }
                if(j==2)
                {
                    System.out.print(m1[i][j] +"}");
                }
                else
                {
                    System.out.print(m1[i][j]+"\t ");
                }
                System.out.println(" ");
            }
        }
    }
}
