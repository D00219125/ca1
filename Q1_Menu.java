package ca1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 *Menu Class 
 * @author Aidan Bligh
 */
public class Q1_Menu {
    
    Scanner kb = new Scanner(System.in);

    /**
     *Switch case that prints options and runs questions
     */
    public void menu()
    {
        int choice = 0;
        while(choice!=6){
        printOptions();
        choice = kb.nextInt();
        switch(choice)
        {
            case 1:
                q2a();
                break;
            case 2:
                q2b();
                break;
            case 3:
                q3();
                break;
            case 4:
                q4();
                break;
            //case 5:
              //  q5();
                //break;
            case 6:
                choice = 6;
                break;
        }
        }
    }
    
    /**
     *Prints a list of the users options.
     */
    public static void printOptions()
    {
        System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Enter 1 for question 2a (the 4X3 matricies addition.)");
        System.out.println("Enter 2 for question 2b (the user input matricies addition.)");
        System.out.println("Enter 3 for question 3 (index of min and max question.)");
        System.out.println("Enter 4 for question 4 (circular shift right)");
        //System.out.println("Enter 5 for question 5 (quiz)");
        System.out.println("Enter 6 to quit.");
        System.out.println("--------------------------------------------------------------------------------");
    }
    
    /**
     *creates 2 4X3 matrices (2d arrays) with randomized values from 0 to 10 and
     * adds them together into a third matrix.
     */
    public void q2a()
    {
        int [][]m1vals = generateMatrixValues(4,3);
        Q2_Matrix matrix1 = new Q2_Matrix(m1vals);
        System.out.println("Matrix 1 :");
        Q2_Matrix.printMatrix(m1vals);
        int [][]m2vals = generateMatrixValues(4,3);
        Q2_Matrix matrix2 = new Q2_Matrix(m2vals);
        System.out.println("Matrix 2 :");
        Q2_Matrix.printMatrix(m2vals);
        int [][] matrix3 = Q2_Matrix.addMatricies(m1vals, m2vals);
        System.out.println("Matrix 3 :");
        Q2_Matrix.printMatrix(matrix3);
    }
    
    /**
     *Creates a matrix (2D array) where the user decides the amount of rows and 
     * columns, The values inside the matrix are random integers between 1 and 
     * 10, the matrix values are added together into a third matrix.
     */
    public void q2b()
    {
        System.out.println("Please enter the amount of rows you want.");
        int rows = kb.nextInt();
        System.out.println("Please enter the amount of columns you want.");
        int columns = kb.nextInt();
         int [][]m1vals = generateMatrixValues(rows,columns);
        Q2_Matrix matrix1 = new Q2_Matrix(m1vals);
        System.out.println("Matrix 1 :");
        Q2_Matrix.printMatrix(m1vals);
        int [][]m2vals = generateMatrixValues(rows,columns);
        Q2_Matrix matrix2 = new Q2_Matrix(m2vals);
        System.out.println("Matrix 2 :");
        Q2_Matrix.printMatrix(m2vals);
        int [][] matrix3 = Q2_Matrix.addMatricies(m1vals, m2vals, rows, columns);
        System.out.println("Matrix 3 :");
        Q2_Matrix.printMatrix(matrix3);
    }
    //this has to be here as I get static issues calling it from q2_matrix

    /**
     *Generates the random integers that fill a matrix(2d array). 
     * @param rows
     * @param columns
     * @return matrix values
     */
    public int [] [] generateMatrixValues(int rows, int columns)
    {
        Random rand = new Random();
        int [][] matrix = new int [rows][columns];
        for(int i = 0; i<rows;i++)
        {
            for(int j = 0; j<columns;j++)
            {
                matrix[i][j] = rand.nextInt(10);
            }
        }
        return matrix;
    }
    
    /**
     *Asks user to fill an array, prints the index of the minimum and maximum
     * value and prints what those values are.
     */
    public void q3()
    {
        Q3_Array q3 = new Q3_Array();
        int [] a = q3.fillArray();
        int [] aIndex = q3.indexOfMinAndMax(a);
        System.out.println("The maximum and minimum index of that array are:");
        System.out.println(Arrays.toString(aIndex));
        System.out.println("The minimum value of the array is " + a[aIndex[0]] + ". The maximum value is " +a[aIndex[1]]);
    }
    
    public void q4()
    {
        Q4_ArrayShift q4 = new Q4_ArrayShift();
        int a[] = q4.fillArray();
        System.out.println("Enter your n index where you like to shift the elements to the right?");
        int n = kb.nextInt();
        int [] result = q4.circularShiftRight(a, n);
        System.out.println(Arrays.toString(result));
    }
    //public void q5()
    //{
     //   Q5_Quiz quiz = new Q5_Quiz();
      //  quiz.askQuestion();
    //}
}
