/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca1part2;

/**
 *
 * @author Aidan Bligh
 */
public class Player {
    private String name;
    private int health;

    /**
     *creates instance of player
     * @param name
     * @param health
     */
    public Player(String name, int health) {
        this.name = name;
        this.health = health;
    }

    /**
     *returns players name as string
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     *returns players health as int
     * @return int health
     */
    public int getHealth() {
        return health;
    }

    /**
     *sets players health
     * @param health
     */
    public void setHealth(int health) {
        this.health = health;
    }
    
    
    
}
